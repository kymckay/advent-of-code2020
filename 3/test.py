import unittest
import os.path
from . import solution

class TestSolution(unittest.TestCase):

    def test_collisions(self):
        # Obtain path relative to script so that it works no matter where ran from
        path = os.path.join(os.path.dirname(__file__), "test_input.txt")

        with open(path, "r") as file:
            terrain = solution.Terrain(file.read())
        
        self.assertEqual(terrain.collisions(3, 1), 7)

if __name__ == '__main__':
    unittest.main()