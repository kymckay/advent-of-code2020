import os.path

class Terrain:
    def __init__(self, str_rep:str):
        self.geology = str_rep.splitlines()
        self.width = len(self.geology[0])

    def collisions(self, sx:int = 1, sy:int = 1) -> int:
        trees = 0
        x = 0 # Initial position

        for y, line in enumerate(self.geology):
            if (y % sy != 0):
                continue

            if (line[x] == "#"):
                trees += 1

            # Pattern is repeated to the right (mod arithmetic)
            # Newline chars removed by splitlines so not in len
            x = (x + sx) % self.width
        
        return trees

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        terrain = Terrain(file.read())

    product = terrain.collisions(3,1)

    print(f"You hit {product} trees at slope (3,1)")

    product *= terrain.collisions() \
        * terrain.collisions(5,1) \
        * terrain.collisions(7,1) \
        * terrain.collisions(1,2)

    print(f"The product of collisions across all given slopes is {product}")

if __name__ == '__main__':
    main()