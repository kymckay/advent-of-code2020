import unittest
import os.path
from . import solution

IN_1 = """[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up
"""

class TestSolution(unittest.TestCase):
    # Only need to read the input file once for all tests
    @classmethod
    def setUpClass(cls):
        # Obtain path relative to script so that it works no matter where ran from
        path = os.path.join(os.path.dirname(__file__), "input.txt")

        with open(path, "r") as file:
            cls.input = file.read()

    def test_example1(self):
        self.assertEqual(solution.problem1(IN_1), 240)

    def test_problem1(self):
        self.assertEqual(solution.problem1(self.input), 104764)

    def test_example2(self):
        self.assertEqual(solution.problem2(IN_1), 4455)

    def test_problem2(self):
        self.assertEqual(solution.problem2(self.input), 128617)