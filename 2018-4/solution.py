from typing import Callable, Dict, Sequence

# Each guard will consist of a 60 length sequence
# Indices represent the minutes they slept (only ever during midnight)
# Elements represent number of times slept at that minute
Guard = Sequence[int]

def observe_guards(input:str) -> Dict[str, Guard]:
    events = input.splitlines()

    # Will be chronological sorting due to convenient timestamp format
    events.sort()

    # Guards will be accessed by their ID
    guards: Dict[str, Guard] = dict()
    current_guard = ""
    for event in events:
        _, time, event_id, guard_id, *_ = event.split()
        if event_id == "Guard":
            current_guard = guard_id
            if guard_id not in guards:
                guards[guard_id] = [0] * 60
        elif event_id == "falls":
            slept_from = int(time[3:5])
        elif event_id == "wakes":
            slept_to = int(time[3:5])
            for minute in range(slept_from, slept_to):
                guards[current_guard][minute] += 1
        else:
            pass # Skip any malformed lines

    return guards

def key_of_max(guards:Dict[str, Guard], method:Callable) -> str:
    max_overall = 0
    found = ""

    for id in guards:
        value = method(guards[id])

        if value > max_overall:
            max_overall = value
            found = id

    return found

def problem1(input:str) -> int:
    guards = observe_guards(input)

    # Using sum will find whoever slept the most minutes
    chosen_guard = key_of_max(guards, sum)

    most_frequent_minute = guards[chosen_guard].index(max(guards[chosen_guard]))

    # Remember to trim # from front of ID
    return most_frequent_minute * int(chosen_guard[1:])

def problem2(input:str) -> int:
    guards = observe_guards(input)

    # Using max will find whoever slept a certain minute most often
    chosen_guard = key_of_max(guards, max)

    most_frequent_minute = guards[chosen_guard].index(max(guards[chosen_guard]))

    # Remember to trim # from front of ID
    return most_frequent_minute * int(chosen_guard[1:])
