import unittest
import os.path
from . import solution

class TestSolution(unittest.TestCase):

    # Only need to read the input file once for all tests
    @classmethod
    def setUpClass(cls):
        # Obtain path relative to script so that it works no matter where ran from
        path = os.path.join(os.path.dirname(__file__), "test_input.txt")

        with open(path, "r") as file:
            cls.input = file.read()

    def test_hierarchy(self):
        hr = solution.BagHierarchy(TestSolution.input)
        self.assertEqual(hr["shiny gold"]["dark olive"], 1)
        self.assertEqual(hr["shiny gold"]["vibrant plum"], 2)

    def test_can_contain(self):
        hr = solution.BagHierarchy(TestSolution.input)
        contained_by = hr.count_can_contain("shiny gold")
        self.assertEqual(contained_by, 4)

    def test_must_contain(self):
        hr = solution.BagHierarchy(TestSolution.input)
        must_contain = hr.count_contains("shiny gold")
        self.assertEqual(must_contain, 32)

if __name__ == '__main__':
    unittest.main()