import os.path
from typing import Dict

class BagHierarchy(dict):
    @staticmethod
    def read_contents(can_hold:str) -> Dict[str, int]:
        if can_hold.startswith("no other bags"):
            return dict()

        contents = dict()
        for bag in can_hold.split(", "):
            quantity, desc1, desc2, *_ = bag.split()
            contents[f"{desc1} {desc2}"] = int(quantity)

        return contents

    def __init__(self, input:str):
        super().__init__()

        for line in input.splitlines():
            bag_id, can_hold = line.split(" bags contain ")
            self[bag_id] = BagHierarchy.read_contents(can_hold)

    def can_contain(self, bag_type:str, recursive:bool=True) -> set:
        can_contain = set()
        for k in self:
            if (bag_type in self[k]):
                can_contain.add(k)
                if recursive:
                    can_contain.update(self.can_contain(k))
        return can_contain

    # How many types of bag can contain a given type
    def count_can_contain(self, bag_type:str, recursive:bool=True) -> int:
        return len(self.can_contain(bag_type, recursive))

    def count_contains(self, bag_type:str, recursive:bool=True) -> int:
        count = 0
        for k in self[bag_type]:
            count += self[bag_type][k]
            if recursive:
                # Each level of nesting has a multiplicative effect on the next
                count += self.count_contains(k) * self[bag_type][k]

        return count

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    hr = BagHierarchy(input)

    print(f"A shiny gold bag can be held by {hr.count_can_contain('shiny gold')} other bag colours")
    print(f"A shiny gold bag must hold {hr.count_contains('shiny gold')} other bags in total")

if __name__ == '__main__':
    main()