import unittest
from . import solution

class TestSolution(unittest.TestCase):
    def setUp(self):
        self.ship = solution.Ship(0)
        self.ship2 = solution.WaypointShip(0, 0, 1)

    def test_move(self):
        self.ship.move(20)
        self.assertEqual(self.ship.x, 0)
        self.assertEqual(self.ship.y, 20)

        self.ship.heading = 90
        self.ship.move(20)
        self.assertEqual(self.ship.x, 20)
        self.assertEqual(self.ship.y, 20)

        self.ship.heading = 180
        self.ship.move(20)
        self.assertEqual(self.ship.x, 20)
        self.assertEqual(self.ship.y, 0)

        self.ship.heading = 270
        self.ship.move(20)
        self.assertEqual(self.ship.x, 0)
        self.assertEqual(self.ship.y, 0)

    def test_rotate(self):
        self.ship.rotate(90)
        self.assertEqual(self.ship.heading, 90)

        self.ship.rotate(-90)
        self.assertEqual(self.ship.heading, 0)

        self.ship.rotate(450)
        self.assertEqual(self.ship.heading, 90)

    def test_interpret(self):
        # Move 20 units north
        self.ship.interpret("N", 20)
        self.assertEqual(self.ship.x, 0)
        self.assertEqual(self.ship.y, 20)

        # Move 50 units south
        self.ship.interpret("S", 50)
        self.assertEqual(self.ship.x, 0)
        self.assertEqual(self.ship.y, -30)

        # Move 10 units east
        self.ship.interpret("E", 10)
        self.assertEqual(self.ship.x, 10)
        self.assertEqual(self.ship.y, -30)

        # Move 30 units west
        self.ship.interpret("W", 30)
        self.assertEqual(self.ship.x, -20)
        self.assertEqual(self.ship.y, -30)

        # Move 50 units in the durrent direction (still north)
        self.ship.interpret("F", 50)
        self.assertEqual(self.ship.x, -20)
        self.assertEqual(self.ship.y, 20)

        # Turn right
        self.ship.interpret("R", 90)
        self.ship.interpret("F", 100)
        self.assertEqual(self.ship.heading, 90)
        self.assertEqual(self.ship.x, 80)
        self.assertEqual(self.ship.y, 20)

        # Turn left
        self.ship.interpret("L", 180)
        self.ship.interpret("F", 60)
        self.assertEqual(self.ship.heading, 270)
        self.assertEqual(self.ship.x, 20)
        self.assertEqual(self.ship.y, 20)

    def test_interpret_all(self):
        self.ship.heading = 90 # Starts east
        distance = self.ship.interpret_all("F10\nN3\nF7\nR90\nF11")
        self.assertEqual(self.ship.x, 17)
        self.assertEqual(self.ship.y, -8)
        self.assertEqual(distance, 25)

    def test_distance(self):
        self.ship.x = -30
        self.ship.y = 10
        self.assertEqual(self.ship.manhattan_distance(), 40)

    def test_move_waypoint(self):
        self.ship2.move_waypoint(2, 0)
        self.assertEqual(self.ship2.wp_x, 2)
        self.assertEqual(self.ship2.wp_y, 1)

        self.ship2.move_waypoint(0, 5)
        self.assertEqual(self.ship2.wp_x, 2)
        self.assertEqual(self.ship2.wp_y, 6)

    def test_rotate_waypoint(self):
        self.ship2.rotate_waypoint(90)
        self.assertEqual(self.ship2.wp_x, 1)
        self.assertEqual(self.ship2.wp_y, 0)

        self.ship2.rotate_waypoint(-90)
        self.assertEqual(self.ship2.wp_x, 0)
        self.assertEqual(self.ship2.wp_y, 1)

    def test_move_to_waypoint(self):
        self.ship2.wp_x = 1
        self.ship2.move(2)
        self.assertEqual(self.ship2.x, 2)
        self.assertEqual(self.ship2.y, 2)
        # Waypoint coordinates should remain unchanged
        # Relative coordinate system does not change
        self.assertEqual(self.ship2.wp_x, 1)
        self.assertEqual(self.ship2.wp_y, 1)



if __name__ == '__main__':
    unittest.main()