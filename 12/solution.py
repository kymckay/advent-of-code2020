import os.path
import math

class Ship:
    cardinals = {
        "N": 0,
        "S": 180,
        "E": 90,
        "W": 270,
    }

    # starts east, 0 being north
    def __init__(self, dir:int=90):
        self.x = 0
        self.y = 0
        self.heading = dir % 360 # degrees

    def move(self, distance:int) -> None:
        rads = math.radians(self.heading)
        self.y += round(math.cos(rads) * distance)
        self.x += round(math.sin(rads) * distance)

    def rotate(self, degrees:int) -> None:
        self.heading += degrees
        self.heading %= 360

    def interpret(self, instruction:str, number:int) -> None:
        if instruction in Ship.cardinals:
            # Preserve heading while moving a cardinal direction
            adjustment = Ship.cardinals[instruction] - self.heading
            self.rotate(adjustment)
            self.move(number)
            self.rotate(-adjustment)
        elif instruction in ("L", "R"):
            dir = -1 if instruction == "L" else 1
            self.rotate(number * dir)
        elif instruction == "F":
            self.move(number)

    # Returns manhattan distance after all instructions
    def interpret_all(self, instructions:str) -> int:
        for line in instructions.splitlines():
            self.interpret(line[0], int(line[1:]))
        return self.manhattan_distance()

    def manhattan_distance(self) -> int:
        return abs(self.x) + abs(self.y)

class WaypointShip(Ship):
    cardinals = {
        "N": (0,1),
        "S": (0,-1),
        "E": (1,0),
        "W": (-1,0),
    }

    def __init__(self, dir:int=90, wx:int=10, wy:int=1):
        super().__init__(dir=dir)
        self.wp_x = wx
        self.wp_y = wy

    def move_waypoint(self, x:int, y:int) -> None:
        self.wp_x += x
        self.wp_y += y

    def rotate_waypoint(self, degrees:int) -> None:
        r = math.hypot(self.wp_x, self.wp_y)
        theta = math.atan2(self.wp_y, self.wp_x)
        theta -= math.radians(degrees) # subtract for clockwise
        self.wp_x = round(r * math.cos(theta))
        self.wp_y = round(r * math.sin(theta))

    def move(self, repetitions: int) -> None:
        for _ in range(repetitions):
            self.x += self.wp_x
            self.y += self.wp_y

    def interpret(self, instruction: str, number: int) -> None:
        if instruction in WaypointShip.cardinals:
            x,y = WaypointShip.cardinals[instruction]
            x *= number
            y *= number
            self.move_waypoint(x,y)
        elif instruction in ("L", "R"):
            dir = -1 if instruction == "L" else 1
            self.rotate_waypoint(number * dir)
        elif instruction == "F":
            self.move(number)

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    ship = Ship()
    print(f"Manhattan distance after direct instruction: {ship.interpret_all(input)}")

    ship = WaypointShip()
    print(f"Manhattan distance after waypoint instruction: {ship.interpret_all(input)}")

if __name__ == '__main__':
    main()