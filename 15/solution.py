def at_point(input:str, seek_index:int) -> int:
    last_said = dict()

    # The sequence is 1-indexed as per the rules
    init = input.split(",")
    for i, num in enumerate(init[:-1], 1):
        last_said[int(num)] = i

    # Must track previous number to find next
    prev = int(init[-1])

    # Must track index (associated with each number)
    index = len(init)

    while index < seek_index:
        # Next sequence value is 0 or time since previous appeared
        next = 0
        if prev in last_said:
            next = index - last_said[prev]

        # Update the history with new most recent
        last_said[prev] = index

        # Prepare for next in sequence
        prev = next
        index += 1

    return next

