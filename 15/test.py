import unittest
from unittest.case import expectedFailure
from . import solution

class TestSolution(unittest.TestCase):

    def test_example1(self):
        self.assertEqual(solution.at_point("0,3,6", 2020), 436)

    def test_problem1(self):
        self.assertEqual(solution.at_point("5,1,9,18,13,8,0", 2020), 376)

    # Takes a while to run, don't want to hold up tests
    # def test_problem2(self):
    #     self.assertEqual(solution.at_point("5,1,9,18,13,8,0", 30000000), 323780)

if __name__ == '__main__':
    unittest.main()