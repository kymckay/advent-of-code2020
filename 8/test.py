import unittest
import os.path
from . import solution

class TestSolution(unittest.TestCase):

    # Only need to read the input file once for all tests
    @classmethod
    def setUpClass(cls):
        # Obtain path relative to script so that it works no matter where ran from
        path = os.path.join(os.path.dirname(__file__), "test_input.txt")

        with open(path, "r") as file:
            cls.input = file.read()

    def test_run_success(self):
        bp = solution.BootProgram("jmp +1")
        bp.run()
        # Line number should be +1 past instructions
        self.assertEqual(bp.line_num, 1)

    def test_run_fail(self):
        bp = solution.BootProgram("jmp 0")
        bp.run()
        # Line number should be stack at 0
        self.assertEqual(bp.line_num, 0)

    def test_run_fix(self):
        bp = solution.BootProgram("jmp 0")
        bp.run_and_fix()
        # Line number should be +1 past instructions
        self.assertEqual(bp.line_num, 1)

    def test_inf_loop_detect(self):
        bp = solution.BootProgram(TestSolution.input)
        self.assertEqual(bp.run(), 5)

    def test_inf_loop_fix(self):
        bp = solution.BootProgram(TestSolution.input)
        self.assertEqual(bp.run_and_fix(), 8)

if __name__ == '__main__':
    unittest.main()