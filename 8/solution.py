import os.path

class BootProgram:
    def __init__(self, instructions:str):
        self.lines = instructions.splitlines() # Must remember lines to jump between them
        self.visited = set() # Closed set (line indices act as node identifiers in the graph)
        self.accumulator = 0 # Must track accumulated value
        self.line_num = 0 # Graph entry point
        self.fix = False # Won't try to fix instructions until told to
        self.terminal_node = len(self.lines) # The program succeedes upon reaching this node
        self.stashed = [0, 1, 2] # Holds state of system at a given moment

        # Boot program spec maps instructions to their function
        self.instructions = {
            "acc": self.accumulate,
            "jmp": self.jump,
            "nop": self.nop
        }

    # Used to cache the current state before attempting to fix instructions
    def stash(self, pop:bool=False):
        if pop:
            self.accumulator = self.stashed[0]
            self.visited = self.stashed[1]
            self.line_num = self.stashed[2]
            self.fix = True
        else:
            self.stashed[0] = self.accumulator
            self.stashed[1] = self.visited.copy()
            self.stashed[2] = self.line_num
            self.fix = False

    def accumulate(self, n:int):
        self.accumulator += n
        self.line_num += 1

    def jump(self, n:int):
        # When fixing, simulate nop to see if it fixes the boot
        if self.fix:
            self.stash()
            self.line_num += 1
            self.run()
            if self.line_num == self.terminal_node:
                return
            else:
                self.stash(pop=True)

        self.line_num += n

    def nop(self, n:int):
        # When fixing, simulate jump to see if it fixes the boot
        if self.fix:
            self.stash()
            self.line_num += n
            self.run()
            if self.line_num == self.terminal_node:
                return
            else:
                self.stash(pop=True)

        self.line_num += 1

    # Runs until an instruction loop is formed
    def run(self) -> int:
        # Program will run until a loop is formed
        while self.line_num not in self.visited and self.line_num != self.terminal_node:
            self.visited.add(self.line_num)
            inst, val = self.lines[self.line_num].split()
            self.instructions[inst](int(val))

        return self.accumulator

    # Runs and tries to swap nop and jmp instructions until completion
    def run_and_fix(self) -> int:
        self.fix = True
        return self.run()

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    bp_fail = BootProgram(input)
    bp_fix = BootProgram(input)

    print(f"The acumulator contains {bp_fail.run()} when a loop is formed")
    print(f"The acumulator contains {bp_fix.run_and_fix()} when the program terminates correctly")

if __name__ == '__main__':
    main()