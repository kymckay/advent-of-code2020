import unittest
from . import solution

class TestSolution(unittest.TestCase):

    def setUp(self):
        self.input = "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb"

    def test_gather_answers(self):
        gathered = solution.gather_answers(self.input)
        self.assertEqual(len(gathered), 5) # 5 groups
        self.assertEqual(len(gathered[0]), 1) # 1 persons in group 1
        self.assertEqual(len(gathered[1]), 3) # 3 persons in group 2
        self.assertEqual(len(gathered[2]), 2) # ...
        self.assertEqual(len(gathered[3]), 4) # ...
        self.assertEqual(len(gathered[4]), 1) # ...

    def test_sum_any(self):
        gathered = solution.gather_answers(self.input)
        self.assertEqual(solution.sum_of_any(gathered), 11)

    def test_sum_of_all(self):
        gathered = solution.gather_answers(self.input)
        self.assertEqual(solution.sum_of_all(gathered), 6)

if __name__ == '__main__':
    unittest.main()