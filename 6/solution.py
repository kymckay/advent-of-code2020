import os.path
from typing import List, Set

InvidualAnswers = Set[str]
GroupAnswers = List[InvidualAnswers]
GroupAnswerList = List[GroupAnswers]

def gather_answers(all_answers:str) -> GroupAnswerList:
    # A nested list will represent each group
    grouped_answers = [[]]
    for person in all_answers.splitlines():
        # Set of characters on a line are the individual's answers
        ind_answers = set(person)

        # Empty lines exist between groups
        if (len(ind_answers) == 0):
            grouped_answers.append([])
        else:
            grouped_answers[-1].append(ind_answers)

    return grouped_answers

# Gives the sum of grouped answers for which anyone in the group said yes
def sum_of_any(grouped:GroupAnswerList) -> int:
    count = 0
    for group in grouped:
        count += len(set.union(*group))

    return count

# Gives the sum of grouped answers for which everyone in the group said yes
def sum_of_all(grouped:GroupAnswerList) -> int:
    count = 0
    for group in grouped:
        count += len(set.intersection(*group))

    return count

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    all_answers = gather_answers(input)

    # The sum of all set's sizes is the number of unique (per group) answers totalled
    print(f"Sum of questions to which anyone per group said yes: {sum_of_any(all_answers)}")
    print(f"Su mof questions to which everyone per group said yes: {sum_of_all(all_answers)}")

if __name__ == '__main__':
    main()