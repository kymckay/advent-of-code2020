import os.path
import itertools
from typing import List, Tuple

def find_sum(options:List[int], n:int=2, total:int=2020) -> Tuple[int]:
    possibilities = itertools.combinations(options, n)
    for s in possibilities:
        if sum(s) == total:
            return s

def product(nums:Tuple[int]) -> int:
    prod = 1
    for i in nums:
        prod *= i
    return prod

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    # Input are all integers
    with open(path, "r") as file:
        numbers = [int(x) for x in file]

    pair = find_sum(numbers)
    print(f"Pair that sums to 2020: {pair} with product {product(pair)}")
    trip = find_sum(numbers, n=3)
    print(f"Triple that sums to 2020: {trip} with product {product(trip)}")

if __name__ == '__main__':
    main()