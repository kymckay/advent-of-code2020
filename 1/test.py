import unittest
from . import solution

class TestSolution(unittest.TestCase):

    def test_product(self):
        self.assertEqual(solution.product((2,2)), 4)
        self.assertEqual(solution.product((5,5,4)), 100)

    def test_find_sum(self):
        nums = [1721,979,366,299,675,1456]
        self.assertEqual(solution.find_sum(nums), (1721,299))
        self.assertEqual(solution.find_sum(nums, 3), (979,366,675))

if __name__ == '__main__':
    unittest.main()