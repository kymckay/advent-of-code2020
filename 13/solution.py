import os.path
from math import inf

class BusTimetable:
    def __init__(self, data:str):
        self.sequence = data.split(",")
        self.buses = tuple(int(x) for x in filter(lambda x: x != "x", self.sequence))

    def first_bus(self, depart_from:int) -> int:
        minimum = inf
        id = 0
        for bus in self.buses:
            to_wait = self.wait_time(bus, depart_from)
            if to_wait < minimum:
                minimum = to_wait
                id = bus
        return id

    def wait_time(self, bus_id:int, depart_from:int) -> int:
        # Remainder gives minutes since previous bus at depart time
        # Then subtracting increment (ID) gives length of time "since" next bus (aka negative)
        wait_time = depart_from % bus_id - bus_id

        return -wait_time

    def find_sequence_start(self):
        # Searching for the sequence in steps of the largest interval reduces iterations
        step = max(self.buses)

        # Offsets will be relative to the index of the max in the sequence
        offset = self.sequence.index(str(step))
        offsets = tuple((self.sequence.index(str(x)) - offset, x) for x in self.buses)

        t = step # First candidate at first max interval
        while any((t+dt) % id != 0 for dt, id in offsets):
            t += step

        # Adjust final output to sequence start position
        return t - offset

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    depart_time, tt = input.splitlines()
    depart_time = int(depart_time)
    tt = BusTimetable(tt)

    bus_id = tt.first_bus(depart_time)
    wait_time = tt.wait_time(bus_id, depart_time)
    print(f"The earliest bus ID multiplied by the wait time in minutes: {bus_id * wait_time}")
    print(f"The first timestamp at which all buses aling with their sequence is: {tt.find_sequence_start()}")

if __name__ == '__main__':
    main()