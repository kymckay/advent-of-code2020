import unittest
from . import solution

class TestSolution(unittest.TestCase):
    def setUp(self):
        self.tt = solution.BusTimetable("7,13,x,x,59,x,31,19")

    def test_find_bus(self):
        self.assertEqual(self.tt.first_bus(939), 59)

    def test_wait_time(self):
        self.assertEqual(self.tt.wait_time(59, 939), 5)

    def test_find_sequence(self):
        self.assertEqual(self.tt.find_sequence_start(), 1068781)

        tt = solution.BusTimetable("17,x,13,19")
        self.assertEqual(tt.find_sequence_start(), 3417)

        tt = solution.BusTimetable("67,7,59,61")
        self.assertEqual(tt.find_sequence_start(), 754018)

        tt = solution.BusTimetable("67,x,7,59,61")
        self.assertEqual(tt.find_sequence_start(), 779210)

        tt = solution.BusTimetable("67,7,x,59,61")
        self.assertEqual(tt.find_sequence_start(), 1261476)

        tt = solution.BusTimetable("67,7,x,59,61")
        self.assertEqual(tt.find_sequence_start(), 1261476)

        tt = solution.BusTimetable("1789,37,47,1889")
        self.assertEqual(tt.find_sequence_start(), 1202161486)

if __name__ == '__main__':
    unittest.main()