from typing import Sequence

# No intermediate representation needed due to simplicity of structure
# Form of recursive descent interpretation to handle nested expressions
class Interpreter:
    def __init__(self, tokens:Sequence[str]):
        self.tokens = tokens
        self.i = 0
        self.token = tokens[0]

    def nextToken(self) -> None:
        self.i += 1
        self.token = self.tokens[self.i] if self.i < len(self.tokens) else None

    # calc: num ((PLUS|MUL) num)*
    def calculation(self) -> int:
        result = self.num()

        while self.token in ("+", "*"):
            t = self.token
            self.nextToken()
            if t == "+":
                result += self.num()
            else:
                result *= self.num()

        return result

    # num: LPAR calc RPAR | NUM
    def num(self) -> int:
        if self.token == "(":
            self.nextToken()
            value =  self.calculation()
            self.nextToken()
            return value
        else:
            value = int(self.token)
            self.nextToken()
            return value

class AdvInterpreter(Interpreter):
    # calc: factor (MUL factor)*
    def calculation(self) -> int:
        result = self.factor()

        while self.token == "*":
            t = self.token
            self.nextToken()
            result *= self.factor()

        return result

    # factor: num (PLUS num)*
    def factor(self) -> int:
        result = self.num()

        while self.token == "+":
            t = self.token
            self.nextToken()
            result += self.num()

        return result

def tokenize(expression:str) -> Sequence[str]:
    tokens = []
    token = ""
    for c in expression:
        if c in ("+", "*", "(", ")"):
            if token != "":
                tokens.append(token)
                token = ""

            tokens.append(c)
        elif c.isspace():
            if token != "":
                tokens.append(token)
                token = ""
        else:
            token += c

    if token != "":
        tokens.append(token)
        token = ""

    return tokens

def calc(expression:str) -> int:
    return Interpreter(tokenize(expression)).calculation()

def calc_adv(expression:str) -> int:
    return AdvInterpreter(tokenize(expression)).calculation()