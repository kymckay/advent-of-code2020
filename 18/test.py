import unittest
import os.path
from . import solution

class TestSolution(unittest.TestCase):
    # Only need to read the input file once for all tests
    @classmethod
    def setUpClass(cls):
        # Obtain path relative to script so that it works no matter where ran from
        path = os.path.join(os.path.dirname(__file__), "input.txt")

        with open(path, "r") as file:
            cls.input = file.read()

    def test_example1(self):
        self.assertEqual(solution.calc("2 * 3 + (4 * 5)"), 26)
        self.assertEqual(solution.calc("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 437)
        self.assertEqual(solution.calc("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"), 12240)
        self.assertEqual(solution.calc("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"), 13632)

    def test_problem1(self):
        total = sum(solution.calc(line) for line in self.input.splitlines())
        self.assertEqual(total, 7293529867931)

    def test_example2(self):
        self.assertEqual(solution.calc_adv("1 + (2 * 3) + (4 * (5 + 6))"), 51)
        self.assertEqual(solution.calc_adv("2 * 3 + (4 * 5)"), 46)
        self.assertEqual(solution.calc_adv("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 1445)
        self.assertEqual(solution.calc_adv("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"), 669060)
        self.assertEqual(solution.calc_adv("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"), 23340)

    def test_problem2(self):
        total = sum(solution.calc_adv(line) for line in self.input.splitlines())
        self.assertEqual(total, 60807587180737)

if __name__ == '__main__':
    unittest.main()