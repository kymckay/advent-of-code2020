import unittest
import os.path
from . import solution

class TestSolution(unittest.TestCase):
    # Only need to read the input file once for all tests
    @classmethod
    def setUpClass(cls):
        # Obtain path relative to script so that it works no matter where ran from
        path = os.path.join(os.path.dirname(__file__), "test_input.txt")

        with open(path, "r") as file:
            cls.input = file.read()

    def test_truthyness(self):
        state = solution.SeatState.OCCUPIED
        self.assertTrue(state)

        state = solution.SeatState.EMPTY
        self.assertFalse(state)

        state = solution.SeatState.FLOOR
        self.assertFalse(state)

    # Expect a 10x10 seating area
    def test_setup(self):
        seating = solution.WaitingArea(self.input)
        self.assertEqual(len(seating), 10)
        for i in range(10):
            with self.subTest(i=i):
                self.assertEqual(len(seating[i]), 10)

    def test_occupied(self):
        seating = solution.WaitingArea("LLL\nL..\nLLL")
        self.assertEqual(seating.occupied(), 0)
        for i in range(3):
            seating[i][i] = solution.SeatState.OCCUPIED
            with self.subTest(i=i):
                self.assertEqual(seating.occupied(), i+1)

    def test_occupied_near(self):
        seating = solution.WaitingArea("L#L\n.L#\n..#")
        self.assertEqual(seating.occupied_near(1,1), 3)
        # Check a corner position (outside area should all be considered empty)
        self.assertEqual(seating.occupied_near(0,0), 1)

    def test_visible_from(self):
        seating = solution.WaitingArea("#L#L#\nL...L\n#.L.#\nL...L\n#L#L#")
        self.assertEqual(seating.visible_from(2,2), 8)

        seating = solution.WaitingArea(".....\nL.##L\n#.L.#\nL...L\n.....")
        self.assertEqual(seating.visible_from(2,2), 4)

    def test_update(self):
        seating = solution.WaitingArea("LLL\nLLL\nLLL")
        # First iteration should fill all seats
        self.assertEqual(seating.update(), 9)
        self.assertEqual(seating.occupied(), 9)
        # Second iteration should leave only the 4 corners occupied
        self.assertEqual(seating.update(), 5)
        self.assertEqual(seating.occupied(), 4)

        seating = solution.WaitingArea("LLL\nL#L\nLLL")
        # No seats should change state due to central occupied seat
        self.assertEqual(seating.update(), 0)

    def test_update_sample(self):
        seating = solution.WaitingArea(self.input)

        # All occupied
        self.assertEqual(seating.update(), 71)
        self.assertEqual(seating.occupied(), 71)

        # Seats with adjacent become empty again
        self.assertEqual(seating.update(), 51)
        self.assertEqual(seating.occupied(), 20)

        # Should stabilize after 3 more rounds
        self.assertEqual(seating.update(), 31)
        self.assertEqual(seating.occupied(), 51)

        seating.update() # Mix of state changes on/off at this point
        self.assertEqual(seating.occupied(), 30)

        seating.update()
        self.assertEqual(seating.occupied(), 37)

        self.assertEqual(seating.update(), 0)

    def test_update_sample_visible(self):
        seating = solution.WaitingArea(self.input)

        # All occupied
        self.assertEqual(seating.update(True), 71)
        self.assertEqual(seating.occupied(), 71)

        # Seats which see 5 others become empty again
        self.assertEqual(seating.update(True), 64)
        self.assertEqual(seating.occupied(), 7)

    def test_stabilize(self):
        seating = solution.WaitingArea(self.input)
        self.assertEqual(seating.stabilize(), 37)

        seating = solution.WaitingArea(self.input)
        self.assertEqual(seating.stabilize(visible_rule=True), 26)

if __name__ == '__main__':
    unittest.main()