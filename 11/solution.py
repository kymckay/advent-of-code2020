import os.path
import enum
import itertools
from typing import Tuple

Coordinate = Tuple[int, int]

class SeatState(enum.Enum):
    OCCUPIED = "#"
    EMPTY = "L"
    FLOOR = "."

    def __bool__(self):
        return self == SeatState.OCCUPIED

# Subclass list for indexing, etc.
class WaitingArea(list):
    def __init__(self, initial:str):
        super().__init__()

        seating_rep = initial.splitlines()
        self.height = len(seating_rep)
        self.width = len(seating_rep[0]) # Ignoring case of 0 height

        for row in seating_rep:
            self.append([])
            for c in row:
                self[-1].append(SeatState(c))

    def __str__(self):
        s = []
        for y in range(self.height):
            for x in range(self.width):
                s.append(self[y][x].value)
            s.append("\n")
        return "".join(s)

    def occupied(self) -> int:
        count = 0
        for x in range(self.width):
            for y in range(self.height):
                count += (self[y][x] == SeatState.OCCUPIED)
        return count

    def space_exists(self, x:int, y:int) -> bool:
        return (0 <= y < self.height
            and 0 <= x < self.width)

    def occupied_near(self, x:int, y:int) -> int:
        vicinity = list(itertools.product((x-1, x, x+1), (y-1, y, y+1)))
        vicinity.pop(len(vicinity) // 2) # Ignore central position being checked
        return sum(
            # Need to check index actually exists in the list (Python wraps negatives)
            self.space_exists(xt, yt)
            and self[yt][xt] == SeatState.OCCUPIED for xt,yt in vicinity
        )

    def get_visible(self, x:int, y:int, dx:int, dy:int) -> Coordinate:
        xt, yt = x + dx, y + dy
        while self.space_exists(xt, yt):
            if self[yt][xt] != SeatState.FLOOR:
                return (xt, yt)
            xt, yt = xt + dx, yt + dy

        # If edge of area was reached just return the floor space there
        return (xt - dx, yt - dy)

    def visible_from(self, x:int, y:int) -> int:
        visible = (
            self.get_visible(x, y, -1, -1),
            self.get_visible(x, y, 0, -1),
            self.get_visible(x, y, 1, -1),
            self.get_visible(x, y, 1, 0),
            self.get_visible(x, y, 1, 1),
            self.get_visible(x, y, 0, 1),
            self.get_visible(x, y, -1, 1),
            self.get_visible(x, y, -1, 0),
        )

        # Ignore own position (may be returned for edge seats)
        return sum(not (xt == x and yt == y) and bool(self[yt][xt]) for xt, yt in visible)

    def flip_state(self, x:int, y:int) -> None:
        self[y][x] = SeatState.EMPTY if self[y][x] == SeatState.OCCUPIED else SeatState.OCCUPIED

    # Returns number of seats that changed state
    def update(self, visible_rule=False) -> int:
        # Seats all update at once, so don't change until all evaluated
        to_flip = []
        for x in range(self.width):
            for y in range(self.height):
                state = self[y][x]

                # Don't bother checking nearby for the floor
                if state == SeatState.FLOOR:
                    continue

                vicinity = self.visible_from(x,y) if visible_rule else self.occupied_near(x,y)
                threshold = 5 if visible_rule else 4
                if (
                    (state == SeatState.EMPTY
                    and vicinity == 0)
                    or
                    (state == SeatState.OCCUPIED
                    and vicinity >= threshold)
                ):
                    to_flip.append((x,y))

        for coord in to_flip:
            self.flip_state(*coord)

        return len(to_flip)

    # Returns number of occupied seats once no more state changes occur
    def stabilize(self, visible_rule=False) -> int:
        while self.update(visible_rule):
            pass
        return self.occupied()

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    seating = WaitingArea(input)
    print(f"Occupied seats after adjacent stabilization: {seating.stabilize()}")

    seating = WaitingArea(input)
    print(f"Occupied seats after visible stabilization: {seating.stabilize(True)}")

if __name__ == '__main__':
    main()