import unittest
import os.path
from . import solution

class TestSolution(unittest.TestCase):

    def setUp(self):
        # Consistently a 3x3 space for testing
        self.space = solution.PocketDimension(3, 3)

    def test_3d_case(self):
        # Obtain path relative to script so that it works no matter where ran from
        path = os.path.join(os.path.dirname(__file__), "test_input.txt")

        with open(path, "r") as file:
            input = file.read()

        space = solution.PocketDimension.from_string(input)

        # 11 active after 1 cycle
        self.assertEqual(space.simulate(), 11)

        # 21 active after 2 cycles
        self.assertEqual(space.simulate(), 21)

        # 38 active after 3 cycles
        self.assertEqual(space.simulate(), 38)

        # 112 active after 6 cycles
        self.assertEqual(space.simulate_cycles(3), 112)

    def test_active_count(self):
        # Should handle none active
        self.assertEqual(self.space.active_cubes(), 0)

        # Each activated cube should add to the count
        i = 0
        for x in range(3):
            for y in range(3):
                for z in range(3):
                    self.space.space[x][y][z] = True # TODO: Don't direct access member
                    i += 1
                    with self.subTest(i=i):
                        self.assertEqual(self.space.active_cubes(), i)

    def test_active_flip(self):
        self.space.space[2][2][2] = True
        self.space.space[0][2][0] = True
        self.space.space[0][0][0] = True

        # Central cube should become active, the others become inactive
        self.assertEqual(self.space.simulate(), 1)
        # Remember coordinates shift with each space growth
        self.assertTrue(self.space.get_state((2,2,2)))

    def test_active_persist_2(self):
        # 1x3x1 cuboid active
        self.space.space[0][0][0] = True
        self.space.space[0][1][0] = True
        self.space.space[0][2][0] = True

        # A 3x1x3 cuboid becomes active around the previous central active cube
        # Central active cub should remain active, above and below become inactive
        self.assertEqual(self.space.simulate(), 9)

    def test_active_persist_3(self):
        # 3 pronged corner piece active (half of a 2x2x2 cube)
        self.space.space[2][2][2] = True
        self.space.space[2][2][1] = True
        self.space.space[1][2][2] = True
        self.space.space[2][1][2] = True

        # A 2x2x2 cube becomes active with an extra 2x2x1 on two sides
        # All original cubes stay active
        self.assertEqual(self.space.simulate(), 16)

    def test_adjacent_count(self):
        # Adjecent central cubes should not be counted more than once
        self.space.space[1][0][1] = True
        active = self.space.active_neighbours((1,1,1))
        self.assertEqual(active, 1)

        # Adjecent edge cubes should not be counted more than once
        self.space.space[1][0][2] = True
        active = self.space.active_neighbours((1,1,1))
        self.assertEqual(active, 2)

        # Adjecent corner cubes should not be counted more than once
        self.space.space[2][2][2] = True
        active = self.space.active_neighbours((1,1,1))
        self.assertEqual(active, 3)

        # Checking a cube at edge of space should not error
        active = self.space.active_neighbours((2,2,2))
        self.assertEqual(active, 0)

if __name__ == '__main__':
    unittest.main()