import itertools
import os.path
from typing import Tuple, Union, List

CubeSpace = Union[List['CubeSpace'], List[bool]]

class PocketDimension:
    @staticmethod
    def from_string(input: str, dimensions:int = 3) -> 'PocketDimension':
        # The input 2D slice is always square
        columns = input.split("\n")
        initial_size = len(columns)

        # Pocket space simply modelled as a tensor of booleans (active state of cube there)
        pocket = PocketDimension(dimensions, initial_size)

        # The string input is a 2D slice
        for y, line in enumerate(columns):
            for x, cube in enumerate(line):
                if cube == "#":
                    loc = pocket.space[x][y]
                    
                    # The 2D slice is always in the center of the the remaining dimensions
                    for i in range(dimensions - 3):
                        loc = loc[initial_size // 2]
                    # Final layer holds the active state
                    loc[initial_size // 2] = True

        return pocket

    def __init__(self, dims:int, size:int):
        self.dims = dims # Number of dimensions
        self.size = size # Extent of every dimension (all always equal)
        self.space = self.construct_dimension(dims, size)

    # Generalised form of n dimensional construction using recursion
    def construct_dimension(self, number:int, size:int) -> CubeSpace:
        if (number == 1):
            return [False] * size

        return [self.construct_dimension(number-1, size) for _ in range(size)]
    
    def active_cubes(self) -> int:
        def sum_nested(space):
            active = 0
            for dimension in space:
                if isinstance(dimension, list):
                    active += sum_nested(dimension)
                else:
                    # Lowest level of nesting contains active state booleans
                    active += dimension
            return active

        return sum_nested(self.space)

    def get_state(self, pos:Tuple[int]) -> bool:
        val = self.space
        for d in pos:
            # Cubes outside the space are all inactive
            if d < 0 or d >= self.size:
                return False

            val = val[d]
        return val

    def active_neighbours(self ,pos:Tuple[int]) -> int:
        ranges = [(i-1, i, i+1) for i in pos] # Get adjacent values for each coordinate
        adj = list(itertools.product(*ranges)) # Cartesian product will combine all to find all adjacent coordinates
        adj.pop(len(adj) // 2) # Remove the original coordinate (always central)

        active = 0
        for coord in adj:
            active += self.get_state(coord)

        return active

    def simulate(self) -> int:
        def set_active(space:CubeSpace, coords:Tuple[int]) -> None:
            loc = space
            for i in range(len(coords) - 1):
                loc = loc[coords[i] + 1] # Coordinates shifted to new space origin
            loc[coords[len(coords) - 1] + 1] = True

        # Each dimension can grow at most 2 cubes (1 in each direction)
        next_frame = self.construct_dimension(self.dims, self.size+2)
        
        all_new_coords = [range(-1, self.size+1) for _ in range(self.dims)]

        # The state of every cube in the new space is determined by the
        # state of the old space
        for coords in itertools.product(*all_new_coords):
            # Check surrounding cubes to determine new state
            adjacent = self.active_neighbours(coords)
            is_active = self.get_state(coords)

            if is_active and adjacent in (2,3):
                set_active(next_frame, coords)
            elif not is_active and adjacent == 3:
                set_active(next_frame, coords)

        self.space = next_frame
        self.size += 2

        return self.active_cubes()

    # 6 cycles is the specified boot process
    def simulate_cycles(self, cycles:int = 6) -> int:
        for _ in range(cycles):
            self.simulate()

        return self.active_cubes()

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    space3d = PocketDimension.from_string(input, 3)
    space4d = PocketDimension.from_string(input, 4)
    print(f"In 3D space, there are {space3d.simulate_cycles(6)} active cubes after the six-cycle boot process")
    print(f"In 4D space, there are {space4d.simulate_cycles(6)} active cubes after the six-cycle boot process")

if __name__ == '__main__':
    main()