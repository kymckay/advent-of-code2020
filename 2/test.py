import unittest
from . import solution

class TestSolution(unittest.TestCase):

    def setUp(self):
        self.sample = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc"

    def test_policy_1(self):
        self.assertEqual(solution.count_valid_passwords(self.sample, False), 2)

    def test_policy_2(self):
        self.assertEqual(solution.count_valid_passwords(self.sample, True), 1)

if __name__ == '__main__':
    unittest.main()