import os.path

def count_valid_passwords(input: str, new_policy_rules: bool) -> int:
    valid = 0

    for line in input.split("\n"):
        policy, pword = line.split(": ")
        limits, letter = policy.split()
        lmin, lmax = [int(x) for x in limits.split("-")]

        if new_policy_rules:
            # The policy is 1-indexed, XOR since only one position must have the char
            valid += (pword[lmin - 1] == letter) ^ (pword[lmax - 1] == letter)
        else:
            occurences = pword.count(letter)
            valid += occurences >= lmin and occurences <= lmax

    return valid

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    print(f"Found {count_valid_passwords(input, False)} valid passwords according to policy 1")
    print(f"Found {count_valid_passwords(input, True)} valid passwords according to policy 2")

if __name__ == '__main__':
    main()