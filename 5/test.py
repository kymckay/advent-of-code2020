import unittest
from . import solution

class TestSolution(unittest.TestCase):

    def test_find_seat(self):
        self.assertEqual(solution.find_seat("FBFBBFFRLR"), (44, 5))
        self.assertEqual(solution.find_seat("BFFFBBFRRR"), (70, 7))
        self.assertEqual(solution.find_seat("FFFBBBFRRR"), (14, 7))
        self.assertEqual(solution.find_seat("BBFFBBFRLL"), (102, 4))

    def test_calc_seat_id(self):
        self.assertEqual(solution.seat_id((70, 7)), 567)
        self.assertEqual(solution.seat_id((14, 7)), 119)
        self.assertEqual(solution.seat_id((102, 4)), 820)

if __name__ == '__main__':
    unittest.main()