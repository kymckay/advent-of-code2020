import os.path
from typing import List, Tuple

Seat = Tuple[int]
SeatList = List[Seat]

def find_seat(partitioning:str) -> Seat:
    row_min = 0
    row_max = 127
    col_min = 0
    col_max = 7

    for c in partitioning[:7]:
        # Half rounded up (div by 2 same as bit shift right)
        half_range = (row_max - row_min + 1) >> 1
        if (c == "F"):
            row_max -= half_range
        elif (c == "B"):
            row_min += half_range
    for c in partitioning[7:]:
        # Half rounded up (div by 2 same as bit shift right)
        half_range = (col_max - col_min + 1) >> 1
        if (c == "L"):
            col_max -= half_range
        elif (c == "R"):
            col_min += half_range

    return (row_min, col_min)

def find_all_seats(input:str) -> SeatList:
    all_seats = []

    for line in input.splitlines():
        all_seats.append(find_seat(line))

    return all_seats

def seat_id(seat:Seat) -> int:
    row, col = seat
    return row * 8 + col

def max_seat_id(all_seats:SeatList) -> int:
    all_ids = [seat_id(s) for s in all_seats]
    return max(all_ids)

def find_missing_seat(all_seats:SeatList) -> int:
    all_ids = [seat_id(s) for s in all_seats]

    # Sorting the list enables finding the gap via lookahead
    all_ids.sort()

    for i, id in enumerate(all_ids):
        if (id +2 == all_ids[i+1]):
            missing = id + 1
            return missing

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    all_seats = find_all_seats(input)

    print(f"Max seat ID found: {max_seat_id(all_seats)}")
    print(f"Your missing seat ID: {find_missing_seat(all_seats)}")

if __name__ == '__main__':
    main()