import unittest
from . import solution

class TestSolution(unittest.TestCase):
    input = "35\n20\n15\n25\n47\n40\n62\n55\n65\n95\n102\n117\n150\n182\n127\n219\n299\n277\n309\n576"

    def test_find_invalid(self):
        result = solution.find_oddity(self.input, preamble=5, options=5)
        self.assertEqual(result, 127)

    def test_find_contigious(self):
        nums = [int(x) for x in self.input.splitlines()]
        result = solution.find_contigious_sum(nums, target=127)
        self.assertEqual(result, [15, 25, 47, 40])

    def test_find_weakness(self):
        result = solution.find_weakness(self.input, target=127)
        self.assertEqual(result, 62)

if __name__ == '__main__':
    unittest.main()