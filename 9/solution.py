import os.path
from typing import List, Sequence
from collections import deque
from itertools import combinations


def sums_from_combo(test:int, num_pool:Sequence[int]) -> bool:
    for x, y in combinations(num_pool, 2):
        if x + y == test:
            return True # Short circuit as soon as found
    return False

def find_oddity(sequence:str, preamble:int=25, options:int=25) -> int:
    num_pool = deque()
    for i, line in enumerate(sequence.splitlines()):
        num = int(line)

        if i<preamble:
            num_pool.append(num)
        elif sums_from_combo(num, num_pool):
            num_pool.popleft()
            num_pool.append(num)
        else:
            return num

def find_contigious_sum(nums:Sequence[int], target:int) -> List[int]:
    for i in range(len(nums)):
        for l in range(2, len(nums) + 1):
            slice = nums[i:i+l]
            if sum(slice) == target:
                return slice

def find_weakness(sequence:str, target:int) -> int:
    num_pool = [int(x) for x in sequence.splitlines()]

    range = find_contigious_sum(num_pool, target)

    return max(range) + min(range)

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    first_fail = find_oddity(input, 25, 25)
    weakness = find_weakness(input, first_fail)

    print(f"The first number which does not meet spec is: {first_fail}")
    print(f"The weakness which targets this number is: {weakness}")

if __name__ == '__main__':
    main()