import unittest
import os.path
from . import solution

class TestSolution(unittest.TestCase):

    def test_quick_checking(self):
        # Obtain path relative to script so that it works no matter where ran from
        path = os.path.join(os.path.dirname(__file__), "test_input.txt")

        with open(path, "r") as file:
            data = file.read()

        validator = solution.PassportValidator(data)
        self.assertEqual(validator.count_valid(), 2)

    def test_strict_checking_valid(self):
        # Obtain path relative to script so that it works no matter where ran from
        path = os.path.join(os.path.dirname(__file__), "test_input_invalid.txt")

        with open(path, "r") as file:
            data = file.read()

        validator = solution.PassportValidator(data, True)
        self.assertEqual(validator.count_valid(), 0)

    def test_strict_checking_valid(self):
        # Obtain path relative to script so that it works no matter where ran from
        path = os.path.join(os.path.dirname(__file__), "test_input_valid.txt")

        with open(path, "r") as file:
            data = file.read()

        validator = solution.PassportValidator(data, True)
        self.assertEqual(validator.count_valid(), 4)




if __name__ == '__main__':
    unittest.main()