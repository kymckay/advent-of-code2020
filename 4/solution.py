import os.path
import re

def in_range(n:int, minr:int, maxr:int):
    return n >= minr and n <= maxr

class PassportValidator:
    def __init__(self, data:str, strict:bool=False):
        self.strict = strict

        self.passports = []
        passport = {}
        for line in data.splitlines():
            # Passports seperated by newline
            if (line is ""):
                self.passports.append(passport)
                passport = {}
            else:
                passport.update({x.split(":")[0] : x.split(":")[1] for x in line.split()})
        self.passports.append(passport) # Include the final passport

    # Passport details must match a given format and pass certain rules
    valid_passport = {
        "byr": (re.compile(r"\d{4}"), lambda x: in_range(int(x), 1920, 2002)),
        "iyr": (re.compile(r"\d{4}"), lambda x: in_range(int(x), 2010, 2020)),
        "eyr": (re.compile(r"\d{4}"), lambda x: in_range(int(x), 2020, 2030)),
        "hgt": (re.compile(r"\d+(cm|in)"), lambda x: in_range(int(x[:-2]), 150, 193) if (x[-2:] == "cm") else in_range(int(x[:-2]), 59, 76)),
        "hcl": (re.compile(r"#[0-9a-f]{6}"), lambda _: True),
        "ecl": (re.compile(r"(amb|blu|brn|gry|grn|hzl|oth)"), lambda _: True),
        "pid": (re.compile(r"\d{9}"), lambda _: True)
    }

    def validate(self, passport:dict):
        if not all([k in passport for k in PassportValidator.valid_passport]):
            return False

        # All data must meet specification when strict
        if self.strict:
            for k in PassportValidator.valid_passport:
                pattern, rule = PassportValidator.valid_passport[k]
                if (not pattern.fullmatch(passport[k]) or not rule(passport[k])):
                    return False

        return True

    def count_valid(self) -> int:
        valid = 0
        for passport in self.passports:
            valid += self.validate(passport)
        return valid

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    validator_mk1 = PassportValidator(input)
    validator_mk2 = PassportValidator(input, True)
    print(f"Found {validator_mk1.count_valid()} valid passports with quick checking")
    print(f"Found {validator_mk2.count_valid()} valid passports with strict checking")

if __name__ == '__main__':
    main()