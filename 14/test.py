import unittest
from unittest.case import skip
from . import solution

class TestSolution(unittest.TestCase):

    def setUp(self):
        self.dec = solution.DecoderChip()

    def test_address_store(self):
        self.dec.process("mem[2] = 5")
        self.assertEqual(self.dec[2], 5)

    def test_mask_store(self):
        self.dec.process("mask = XXXXX")
        self.assertSequenceEqual(self.dec.mask, ["X"] * 5)

    def test_mask_apply(self):
        self.dec.update_mask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X")
        # Case where mask makes changes
        self.dec.decode1(0, 11)
        self.assertEqual(self.dec[0], 73)
        # Case where mask results in no changes
        self.dec.decode1(0, 101)
        self.assertEqual(self.dec[0], 101)

    def test_mask_apply_v2(self):
        # Case with two branches
        self.dec.update_mask("000000000000000000000000000000X1001X")
        self.dec.decode2(42, 100)
        self.assertEqual(self.dec[26], 100)
        self.assertEqual(self.dec[27], 100)
        self.assertEqual(self.dec[58], 100)
        self.assertEqual(self.dec[59], 100)
        # Case with three branches
        self.dec.update_mask("00000000000000000000000000000000X0XX")
        self.dec.decode2(26, 1)
        self.assertEqual(self.dec[16], 1)
        self.assertEqual(self.dec[17], 1)
        self.assertEqual(self.dec[18], 1)
        self.assertEqual(self.dec[19], 1)
        self.assertEqual(self.dec[24], 1)
        self.assertEqual(self.dec[25], 1)
        self.assertEqual(self.dec[26], 1)
        self.assertEqual(self.dec[27], 1)
        #  No other addresses should be written
        self.assertEqual(self.dec.process(""), 208)

if __name__ == '__main__':
    unittest.main()