import os.path
from typing import Sequence

# Values are all 36 bits in this case
def dec_to_bin(num:int) -> Sequence[str]:
    return list(f"{num:036b}")

# Dict represents memory (closed world, all missing addresses are value 0)
class DecoderChip(dict):
    def __init__(self):
        super().__init__()
        self.mask = tuple()

    def process(self, instructions:str, v2:bool=False) -> int:
        for line in instructions.splitlines():
            cmd, val = line.split(" = ")
            if cmd == "mask":
                self.update_mask(val)
            else:
                # Strip trailing ]
                _, addr = cmd[:-1].split("[")
                method = self.decode2 if v2 else self.decode1
                method(int(addr), int(val))

        return sum(self.values())

    # Mask will be kept in sequence form and applied logically
    # Bitwise application becomes complicated to reason about due to
    # Python's int type handling (no native unsigned 36 bit int type)
    def update_mask(self, mask:str) -> None:
        self.mask = tuple(mask)

    def decode1(self, addr:int, val:int) -> None:
        binval = dec_to_bin(val)

        # Apply from smallest bit (rightmost) as mask not guarenteed
        # to be same length
        for i in range(-1, -len(self.mask) - 1, -1):
            mask_bit = self.mask[i]
            if mask_bit != "X":
                binval[i] = mask_bit

        self[addr] = int("".join(binval), 2)

    def decode2(self, addr:int, val:int) -> None:
        # List of addresses will grow with each floating bit
        binaddr = [dec_to_bin(addr)]

        # Apply from smallest bit (rightmost) as mask not guarenteed
        # to be same length
        for i in range(-1, -len(self.mask) - 1, -1):
            mask_bit = self.mask[i]

            # Must sequentially apply bit rules to all address branches
            # Iterate backwards to modify while iterating
            j = len(binaddr) - 1
            while j >= 0:
                if mask_bit == "X":
                    binaddr.append(binaddr[j][:])
                    binaddr[j][i] = "1" if binaddr[j][i] == "0" else "0"
                elif mask_bit == "1":
                    binaddr[j][i] = "1"

                j -= 1

        for faddr in binaddr:
            self[int("".join(faddr), 2)] = val

def main():
    # Obtain path relative to script so that it works no matter where ran from
    path = os.path.join(os.path.dirname(__file__), "input.txt")

    with open(path, "r") as file:
        input = file.read()

    dc = DecoderChip()
    print(f"The sum of address values after instructions is: {dc.process(input)}")
    dc = DecoderChip()
    print(f"The sum of address values after v2 instructions is: {dc.process(input, True)}")

if __name__ == '__main__':
    main()